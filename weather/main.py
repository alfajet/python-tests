import datetime
import json
import logging
import os
from io import StringIO
from pathlib import Path
from typing import Any, Dict, Optional

import numpy as np
import requests
import typer

logging.basicConfig()

app = typer.Typer()

CBG_WEATHER_FQDN = "www.cl.cam.ac.uk"
daily_report_endpoint = f"https://{CBG_WEATHER_FQDN}/research/dtg/weather/daily-text.cgi"


row_dt = [
    ("time", "U5"),
    ("temperature", float),
    ("humidity", float),
    ("dew_point", float),
    ("pressure", float),
    ("wind_speed", float),
    ("wind_dir", "U2"),
    ("sun_hours", float),
    ("rain", float),
    ("start_time", "U5"),
    ("max_wind_speed", int),
]


def get_daily_report(report_date: datetime.date) -> str:
    response = requests.get(url=daily_report_endpoint, params=report_date.isoformat())
    response.raise_for_status()
    return response.text


def get_stats(data: np.ndarray, column_name: str) -> Dict[str, float]:
    col = data[column_name]
    return {"mean": np.mean(col), "min": np.min(col), "max": np.max(col)}


def process_report(raw_report: str, report_date: datetime.date) -> Dict[str, Any]:
    data = np.loadtxt(StringIO(raw_report), dtype=row_dt, delimiter="\t", comments="#")
    stats = {prop: get_stats(data, prop) for prop in ["temperature", "humidity", "dew_point", "pressure"]}
    return {"date": report_date.isoformat(), "stats": stats, "raw_data": data.tolist()}


@app.command()
def source_daily(
    export_dir: Optional[Path] = typer.Argument(None),
    report_date: Optional[datetime.datetime] = typer.Option(None, formats=["%Y-%m-%d"]),
):
    export_dir = export_dir if export_dir else Path(os.getcwd())
    # Note: Casting `datetime` as `date` since typer doesn't support yet date types.
    report_date = report_date.date() if report_date else datetime.date.today()
    if report_date > datetime.date.today():
        logging.error(
            "'%s' is a date in the future, please use a past date.",
            report_date.isoformat(),
        )
        raise typer.Exit(code=1)
    export_file = export_dir / f"{report_date.isoformat()}.json"
    raw_report = get_daily_report(report_date)
    processed_report = process_report(raw_report, report_date)
    export_file.write_text(json.dumps(processed_report, indent=2))


if __name__ == "__main__":
    app()
