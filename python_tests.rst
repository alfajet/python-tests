Testing a python project
========================

100% coverage is not enough
---------------------------

Alexandre (Alex) Faget

Campug — 2022-05-03

* Repo: https://framagit.org/alfajet/python-tests
* Slides: https://alfajet.frama.io/python-tests

|cc-attr-sharealike| This presentation and related content are shared under a `Creative Commons Attribution-ShareAlike 4.0 International License`_.

.. |cc-attr-sharealike| image:: images/cc-attribution-sharealike-88x31.png
   :alt: CC-Attribution-ShareAlike image

.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/

----

Foreword
========

The aim of this presentation is to outline things that I found useful to improving by testing & development practices.

So, you will read here are my opinions, which have been formed by my experience.

Hopefully this will be useful to junior developers and a refresher for the more senior ones.

More importantly I wish this will be a starting point for discussion.

----

Outline
=======

* Is it OK not to test? Why testing?

* Some considerations on how to test

* Example: Automate weather data collection & simple stats calculation

   * Short intro to pytest
   * Presentation of some helpful testing helpers: `responses`_, `testfixtures`_ & `time-machine`_.

* Wrap-up, Q&A

----

Is it OK not to test?
=====================

Short answer: yes, but...

Long answer:

- When writing a trivial script for yourself or throwaway code
- When spiking something
- In early stage of a project when scope and design decision are not yet stabilised, having too many tests
could be "in the way".

However:

- Be mindful of technical debt:
    - Diving too deeply in implementation without testing will be hard to catch-up
    - Some implicit decisions might not be well reflected by tests

----

Why testing your application?
=============================

Obvious reason: reliability

Less obvious reasons:
  - Let others and your future self understand what your project is about: tests **are** your app requirements.
  - Good tests should show what your app is about, how it runs, what are the expected inputs, etc.
  - As the project grows, it will help getting confidence when refactoring or adding new features.

----

How to test? (1/2)
==================

My background is mechanical engineering: testing is a core part of product development.
Some tests are meant to break things (e.g. tensile testing, crash tests), some others less so (scans): but the point is
they are **challenging** the product in as many ways as possible.

Software testing should not be different on that respect. Tests should not be complacent. Are you challenging enough
your app?

Also, compared to testing real things, software testing is relatively cheap.

----

How to test? (2/2)
==================

Unlike real objects, software are easy to change:

- No need for big design or detailed planning upfront. In most cases, this will be counterproductive.
   - This could lead to YAGNI
- Implement incrementally

=> `Test Driven Development`_ principles have been founded on these principles.



----

Testing in practice: Weather data
=================================

Objective:

* Get daily reports (txt) from Cambridge University's `Digital Technology Group station`_
* Parse its content
* Calculate Average, Min, Max values of "non-accumulative" properties (Temperature, Pressure, etc.)
* Get Max value of "accumulative" properties (rain, sun hours)


----

Running pytest
==============

If you have cloned this repo & prepared your environment.

Run pytest from a terminal

::

        $ pytest

        ======================================= test session starts =======================================
        platform linux -- Python 3.10.4, pytest-7.1.1, pluggy-1.0.0
        rootdir: /home/alex/projects/campug/python-tests
        plugins: time-machine-2.6.0
        collected 12 items

        test_weather/test_main.py ............                                                      [100%]

        ======================================= 12 passed in 0.23s ========================================

----

Running pytest
==============

Now with -v flag to get the detailed breakdown:


::

        $ pytest -v
        =================================================== test session starts ====================================================
        platform linux -- Python 3.10.4, pytest-7.1.1, pluggy-1.0.0 -- /home/alex/.cache/pypoetry/virtualenvs/python-tests-q587zQn_-py3.10/bin/python
        cachedir: .pytest_cache
        rootdir: /home/alex/projects/campug/python-tests
        plugins: time-machine-2.6.0
        collected 12 items

        test_weather/test_main.py::TestSourceDaily::test_main_with_default_args PASSED                                       [  8%]
        test_weather/test_main.py::TestSourceDaily::test_main_check_stats_props PASSED                                       [ 16%]
        test_weather/test_main.py::TestSourceDaily::test_main_check_stats_values[temperature] PASSED                         [ 25%]
        test_weather/test_main.py::TestSourceDaily::test_main_check_stats_values[pressure] PASSED                            [ 33%]
        test_weather/test_main.py::TestSourceDaily::test_main_check_stats_values[humidity] PASSED                            [ 41%]
        test_weather/test_main.py::TestSourceDaily::test_main_check_stats_values[dew_point] PASSED                           [ 50%]
        test_weather/test_main.py::TestSourceDaily::test_main_without_export_path PASSED                                     [ 58%]
        test_weather/test_main.py::TestSourceDaily::test_main_with_past_date_check_request_param PASSED                      [ 66%]
        test_weather/test_main.py::TestSourceDaily::test_main_with_current_date_check_request_param PASSED                   [ 75%]
        test_weather/test_main.py::TestSourceDaily::test_main_with_past_date_check_report PASSED                             [ 83%]
        test_weather/test_main.py::TestSourceDaily::test_main_with_current_date_check_report PASSED                          [ 91%]
        test_weather/test_main.py::TestSourceDaily::test_main_with_future_date PASSED                                        [100%]

        ==================================================== 12 passed in 0.23s ====================================================


Or use your IDE testing facilities


----

Getting started with pytest
===========================

Tests discovery, basic rules:

* pytest will run modules named `test_*.py` or `*_test.py`
* test functions / methods should start with `test_`
* tests can be grouped in test classes prefixed with `Test`
* Check pytest `test discovery`_

----

Getting started with pytest
===========================

One of the key strengths of pytest is its fixtures:

* reusable (ensures tests don't affect each others)
* can request other fixtures (composition)
* Teardown / Cleanup:

   - Use "yield" instead of return
   - Clean-up actions are writen after yield statement

----

Getting started with pytest
===========================

Example:

.. code-block:: python

        @pytest.fixture
        def current_work_dir(tmp_path):
            real_cwd = os.getcwd()
            os.chdir(tmp_path)
            yield tmp_path
            os.chdir(real_cwd)

* This fixture changes the current working dir to a temp directory
* The path is yielded to the caller
* As a clean-up, the original working dir is restored

----

Testing Approach
================

Understand the boundaries of your code.

* For not simple / medium complex cases, end-to-end testing against `main` (akin to Functional Testing)
* If you write library code (internal to your project or public), test the public API

Benefits of the approach:

* Exercise the whole code, ensure that the different components work together as expected.
* Can be used as a runner to quickly check a change
* More freedom in refactoring the internals -- this shouldn't break existing tests

----

Testing Approach
================

Challenges:

* How do we interact with the outer world?

  * my app expect files from a specific drive location
  * it makes requests to a REST API,
  * it needs inputs from the user.

* Feeding one internal object in a specific way could be hard.
* It's difficult to get feedback, perhaps this is a sign that your app needs
  to supply more info (logging, console outputs, etc.)

----

Let's be serious, mock the world
================================

When the inners of your app needs to interact with the outer world, this is where we need to "mock" these interactions. Mocks are entities that simulates the behaviour of an other entity.

Good use cases for mocks:

- Faking third party APIs
- Time
- Responses from the operating environment

However, never mock the internals of your app. If necessary, it usually a symptom of bad design.


----

Mock http requests
==================

If your app makes a call to a third party service, you don't want your unit test to
rely on it. It's a very good candidate for mocking.

But how do we do it?

Let's have a look at the demo weather app.

1. The app gets the date to request
2. It forms the relevant url, with the date passed as parameter
3. It makes the request
4. Then process the response

What we need to mock is what happens between 3 & 4

If you use requests_, there is a really nice and powerful to mock it: responses_!


----

responses in action
===================

In our code, we're making requests against the daily text reports endpoint:

E.g. https://www.cl.cam.ac.uk/research/dtg/weather/daily-text.cgi?2022-04-22

What we need to do is:

1. Activate responses, usually by decorating the test using it

.. code-block:: python

    @responses.activate
    def test_main_with_default_args():
        ...

2. Add responses for the expected http requests

.. code-block:: python

    responses.add(
        responses.GET,
        daily_report_endpoint,
        status=200,
        body=(fixture_dir / "valid_daily_report.txt").read_text(),
    )

That's it!

----

Short exercise: handle DTG weather failures
===========================================

Assignment:

* Add a new test (or more?) whose objective is to check the app behaviour on DTG internal errors (i.e. response 5xx):

   - The app should handle this situation gracefully
   - Nature of the error should be reported to the user
   - The app should terminate with a non-zero exit code to signal non success to the shell

----

For the posterity
=================

If your project gets serious logging will likely be an important part of your application. And therefore they should be tested.

With `LogCapture`, `testfixtures`_ offers a great way to intercept them and test them accordingly.

The simplest way to use it as a context manager, and run the tested code inside it.

.. code-block:: python

        def test_logs():
            with LogCapture() as logs:
                tested_fn()

            assert "expected log" in str(logs)


----

For the posterity
=================

Some tips on `LogCapture`:

  * filter logs by level (e.g. `LogCapture(level=logging.ERROR)` to ensure the checked logged has the right level
  * For a broad check on text presence, the expected string could be compared with the `str` representation of the logs objects
  * More detailed assertions can be done using `check()` (compare all log entries) and `check_present()` (only specified log entries) methods.

----

It's about time!
================

If you need to test behaviour that are time sensitive, `time-machine`_ is a simple, easy to use mock.

In most cases you can simply use it as a decorator for your tests, example:

.. code-block:: python

    test_date = datetime.date(2022, 4, 8)

    @time_machine.travel(test_date)
    def test_time_sensitive_behaviour():
        assert datetime.date.today() == self.test_date



----

It's about time!
================

* Time can can be completely frozen (`tick=False`)
* Can be used as a decorator, context manager, in a fixture, depending on the use cases
* `move_to()` and `shift()` allow for fine control
* It handles timezones


----

Time's running out!
===================

* Testing is an investment

  * It's hard initially
  * It could seem to go in the way initially
  * It pays off over time: refactoring, scope increase
  * Good tests will help getting contributions on your project

* Developing with testing in mind will help bringing clarity

  * Something is difficult to test: perhaps requirements are unclear? Objects are coupled?

Thank you for your time


----

Questions?
==========


----

Useful libraries
================

* If you're making requests_, I might have some responses_...
* If you're writing files, logging stuff, testfixtures_ might help you
* What about a `time-machine`_?

.. _requests: https://docs.python-requests.org/en/latest/
.. _responses: https://github.com/getsentry/responses
.. _testfixtures: https://testfixtures.readthedocs.io/en/latest/index.html
.. _time-machine: https://github.com/adamchainz/time-machine
.. _doctest: https://docs.python.org/3/library/doctest.html
.. _Test Driven Development: https://en.wikipedia.org/wiki/Test-driven_development
.. _Digital Technology Group station: https://www.cl.cam.ac.uk/research/dtg/weather/
.. _test discovery: https://docs.pytest.org/en/7.0.x/explanation/goodpractices.html#conventions-for-python-test-discovery