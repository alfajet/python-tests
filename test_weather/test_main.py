import datetime
import json
import logging
import os
from pathlib import Path

import pytest
import responses
import time_machine
from testfixtures import LogCapture
from typer.testing import CliRunner

from weather.main import app, daily_report_endpoint

fixture_dir = Path(__file__).parent / "fixtures"


@pytest.fixture
def add_default_response():
    responses.add(
        responses.GET,
        daily_report_endpoint,
        status=200,
        body=(fixture_dir / "valid_daily_report.txt").read_text(),
    )


@pytest.fixture
def current_work_dir(tmp_path):
    real_cwd = os.getcwd()
    os.chdir(tmp_path)
    yield tmp_path
    os.chdir(real_cwd)


class TestSourceDaily:
    test_date = datetime.date(2022, 4, 8)
    expected_stats = {
        "temperature": {
            "min": 2.5,
            "mean": 5.0,
            "max": 7.5,
        },
        "pressure": {
            "min": 995,
            "mean": pytest.approx(1003.4, rel=1e-1),
            "max": 1020,
        },
        "humidity": {
            "min": 50,
            "mean": pytest.approx(66.7, rel=1e-1),
            "max": 80,
        },
        "dew_point": {
            "min": -1.0,
            "mean": pytest.approx(0.67, rel=1e-2),
            "max": 3.0,
        },
    }

    @responses.activate
    @time_machine.travel(test_date)
    def test_main_with_default_args(self, tmp_path, add_default_response):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily with export dir
        Expectations:
            - Main returns exit code 0
            - File is downloaded into provided export directory
        """
        expected_file = tmp_path / f"{datetime.date.today().isoformat()}.json"
        runner = CliRunner()

        results = runner.invoke(app, [str(tmp_path)])

        assert results.exit_code == 0
        assert expected_file.exists()

    @responses.activate
    @time_machine.travel(test_date)
    def test_main_check_stats_props(self, tmp_path, add_default_response):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily with export dir
        Expectations:
            - Properties are present in the stats dictionary
        """
        expected_file = tmp_path / f"{datetime.date.today().isoformat()}.json"

        CliRunner().invoke(app, [str(tmp_path)])

        data = json.load(expected_file.open(mode="r"))
        assert sorted(data["stats"]) == sorted(["temperature", "humidity", "dew_point", "pressure"])

    @responses.activate
    @time_machine.travel(test_date)
    @pytest.mark.parametrize("prop", ["temperature", "pressure", "humidity", "dew_point"])
    def test_main_check_stats_values(self, tmp_path, add_default_response, prop):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily with export dir
        Expectations:
            - statistic values are correctly calculated
        """
        expected_file = tmp_path / f"{datetime.date.today().isoformat()}.json"

        CliRunner().invoke(app, [str(tmp_path)])

        data = json.load(expected_file.open(mode="r"))
        for stat_type in self.expected_stats[prop]:
            assert data["stats"][prop][stat_type] == self.expected_stats[prop][stat_type]

    @responses.activate
    @time_machine.travel(test_date)
    def test_main_without_export_path(self, add_default_response, current_work_dir):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily without export dir
        Expectations:
            - Data exported to current working dir
        """
        expected_file = current_work_dir / f"{datetime.date.today().isoformat()}.json"

        CliRunner().invoke(app, [])

        assert expected_file.exists()

    @responses.activate
    def test_main_with_past_date_check_request_param(
        self, add_default_response, current_work_dir
    ):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily with past date
        Expectations:
            - Date correctly passed to the request parameters
        """
        requested_date = "2020-01-01"

        CliRunner().invoke(app, ["--report-date", requested_date])

        assert responses.assert_call_count(f"{daily_report_endpoint}?{requested_date}", 1)

    @responses.activate
    @time_machine.travel(test_date)
    def test_main_with_current_date_check_request_param(
        self, add_default_response, current_work_dir
    ):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily with today's date
        Expectations:
            - Date correctly passed to the request parameters
        """
        CliRunner().invoke(app, [])

        assert responses.assert_call_count(f"{daily_report_endpoint}?{datetime.date.today().isoformat()}", 1)

    @responses.activate
    def test_main_with_past_date_check_report(
        self, add_default_response, current_work_dir
    ):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily with past date
        Expectations:
            - File named after the past date
            - Date correctly added to the report
        """
        requested_date = "2020-01-01"
        expected_file = current_work_dir / f"{requested_date}.json"

        CliRunner().invoke(app, ["--report-date", requested_date])

        assert expected_file.is_file()
        data = json.load(expected_file.open("r"))
        assert data["date"] == requested_date

    @responses.activate
    @time_machine.travel(test_date)
    def test_main_with_current_date_check_report(
        self, add_default_response, current_work_dir
    ):  # pylint: disable=no-self-use
        """
        Objective:
            - Test source_daily with current date
        Expectations:
            - File named after the current date
            - Date correctly added to the report
        """
        today_iso = datetime.date.today().isoformat()
        expected_file = current_work_dir / f"{today_iso}.json"

        CliRunner().invoke(app, [])

        assert expected_file.is_file()
        data = json.load(expected_file.open("r"))
        assert data["date"] == today_iso

    @responses.activate
    @time_machine.travel(test_date)
    def test_main_with_future_date(self, current_work_dir):  # pylint: disable=no-self-use
        future_date = "2025-01-31"

        with LogCapture(level=logging.ERROR) as logs:
            results = CliRunner().invoke(app, ["--report-date", future_date])

        assert results.exit_code == 1
        assert f"'{future_date}' is a date in the future, please use a past date." in str(logs)
