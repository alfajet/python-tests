========================
Testing python projects
========================

Written as a talk for CamPUG_

History
~~~~~~~

The talk was (will be) given at CamPUG_ on 2022-05-03.

.. _CamPUG: https://www.meetup.com/CamPUG/

Running the examples
~~~~~~~~~~~~~~~~~~~~

This requires a recent version of Python (tested with Python 3.10).

Clone this repository, and ``cd`` into its directory.

If you have poetry_, then set up the environment and dependencies as follows::

  $ poetry shell
  $ poetry install

.. _poetry: https://python-poetry.org/


Otherwise, create a new virtual environment and install dependencies declared in `requirements.txt`::

  $ pip install -r "requirements.txt"


Presentation
~~~~~~~~~~~~

Slides available at https://alfajet.frama.io/python-tests

--------

  |cc-attr-sharealike|

  This presentation and related content are shared under a `Creative Commons
  Attribution-ShareAlike 4.0 International License`_.

.. |cc-attr-sharealike| image:: images/cc-attribution-sharealike-88x31.png
   :alt: CC-Attribution-ShareAlike image

.. _`Creative Commons Attribution-ShareAlike 4.0 International License`: http://creativecommons.org/licenses/by-sa/4.0/
